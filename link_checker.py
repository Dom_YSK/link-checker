import aiohttp
import asyncio
import async_timeout
import json

'''
example Json {"https://google.com": {"final_url":"google.co.uk", "status":200, "history": [302]}}
'''


def url_list_loader(json_file: str)-> dict:
    with open(json_file, 'r') as f:
        urls = json.loads(f.read())
    return urls

async def fetch(session, url):
    async with async_timeout.timeout(10):
        async with session.get(url) as response:
            print(response.status)
            return response


async def checker(urls):
    '''
    TODO: Dominic change prints to logging, colourised?
    :param urls:
    :return:
    '''
    for url in urls:
        async with aiohttp.ClientSession() as session:
            response = await fetch(session, url)
            if urls[url]["final_url"] in str(response.url):
                print(f"{url} resolved correctly.")
            else:
                print(f"{url} did not end at {urls[url]['final_url']}.")
            if response.status == urls[url]["status"]:
                print(f"{url} correctly returned {response.status}.")
            else:
                print(f"{url} returned status {response.status} instead of {urls[url][status]}.")
            if response.history:
                for x in range(len(response.history)):
                    if response.history[x-1].status == urls[url]["history"][x-1]:
                        print(f"History at index {x} is correct")
                    else:
                        print(f"History at index {x} of {response.history[x-1].status} did not"
                              f" match expected value of {urls[url]['history'][x-1]}")


if __name__ == "__main__":
    # you can change this json file to point to where you like,
    # TODO: Dominic arg parse this?
    urls = url_list_loader('urls.json')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(checker(urls))
